﻿import json
import os,sys

from time import gmtime, strftime
def Test1():
  with open('../InputJson/TestDatas.json') as env_data:
    env_details = json.load(env_data)
  env_data.close()
  testfilename = "testfile_"+strftime("%Y_%m_%d_%H_%M_%S", gmtime())
  TestedApps.notepad.Run(1, True)
  edit = Aliases.notepad.wndNotepad.Edit
  for value in env_details.values():
    Aliases.notepad.wndNotepad.Edit.Keys(value)
    aqUtils.Delay(2000)
    edit.Keys("[Enter]")
  stringval = edit.Keys("^a^c^c^c")
  wndNotepad = Aliases.notepad.wndNotepad
  aqUtils.Delay(3000)
  wndNotepad.MainMenu.Click("File|Save As...")
  aqUtils.Delay(3000)
  dlgSaveAs = Aliases.notepad.dlgSaveAs
  aqUtils.Delay(3000)
  dlgSaveAs.DUIViewWndClassName.Explorer_Pane.FloatNotifySink.ComboBox.SetText(testfilename)
  aqUtils.Delay(3000)
  dlgSaveAs.btnSave.ClickButton()
  aqUtils.Delay(2000)
  wndNotepad.Close()
  #verifiction part
  mydict = {'Script Status': [], 'Datas Used for Test': []}
  if(os.path.exists('D:\\Medecision Transition\\Documents\\TestComplete 14 Projects\\'+testfilename+".txt")):
    Log.Message("File Saved Successfully")
    mydict['Script Status'].append("PASS")
    pass
  else:
    status="FAIL"
    mydict['Script Status'].append("FAIL")
    Log.Message("Unable to save the edited notepad file")
  mydict['Datas Used for Test'].append(env_details)
  with open('../OutputJson/TestResult.json','w') as js:
      json.dump(mydict,js,indent=2)
  
  sys.path.append("C:\\Program Files (x86)\\SmartBear\\TestComplete 14\\Bin\\Extensions\\Python\\")
  mysqlpackage_location = "C:\\Program Files (x86)\\SmartBear\\TestComplete 14\\Bin\\Extensions\\Python\\Python36\\Lib\\site-packages"
  Log.Message(mysqlpackage_location)
  sys.path.insert(0, mysqlpackage_location)
  import mysql.connector
  mydb = mysql.connector.connect(host="localhost",user="root",passwd="Test123#")
  mycursor = mydb.cursor()
  sql = "INSERT INTO testdatas.reports (ReportResult, ReportDataUsed,LastUpdatedTime) VALUES (%s, %s, %s);"
  val = (str(mydict['Script Status']), str(mydict['Datas Used for Test']),str(strftime("%Y:%m:%d: %H:%M:%S", gmtime())))
  mycursor.execute(sql, val)
  mydb.commit()
  Log.Message(mycursor.rowcount, "record inserted.")
  

